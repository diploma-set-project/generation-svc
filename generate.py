import codecs
import glob
import json
import random
import string
import os
import sys
import webcolors
import cv2
import numpy as np
from PIL import ImageFont, ImageDraw, Image


def generate(blur_size, is_dark_bg):

    bg = 255
    bgTitle = 'white'

    if (is_dark_bg):
        bgTitle = 'black'
        bg = 0

    img = np.zeros(shape=(250, 600, 3), dtype=np.uint8)

    cv2.waitKey()
    cv2.destroyAllWindows()

    size = random.randint(80, 120)
    length = random.randint(4, 8)
    img_pil = Image.fromarray(img + bg)

    fonts = glob.glob("*.ttf")

    # Drawing text and lines
    font = ImageFont.truetype(random.choice(fonts), size)
    draw = ImageDraw.Draw(img_pil)
    text = ''.join(
        random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase)
        for _ in range(length))
    r = random.randint(0, 255)
    g = random.randint(0, 255)
    b = random.randint(0, 255)
    draw.text((5, 10), text, font=font,
              fill=(b, g, r))
    draw.line([(random.choice(range(length * size)), random.choice(range((size * 2) + 5)))
                  , (random.choice(range(length * size)), random.choice(range((size * 2) + 5)))]
              , width=1, fill=(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)))

    # Adding noise and blur
    img = np.array(img_pil)
    thresh = random.randint(1, 5) / 100
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            rdn = random.random()
            if rdn < thresh:
                img[i][j] = random.randint(0, 123)
            elif rdn > 1 - thresh:
                img[i][j] = random.randint(123, 255)
    return {
        'text': text,
        'image': cv2.blur(img, (int(size / blur_size), int(size / blur_size))),
        'color': get_colour_name((r, g, b))[1],
        'bg': bgTitle
    }


array = []


def generate_set_images(folder, blur_size, amount):
    global array
    for i in range(0, amount):
        is_dark_bg = False
        if (i >= amount/2):
            is_dark_bg = True
        text_image = generate(blur_size, is_dark_bg)
        filepath = './' + folder + '/' + str(i) + '.png'
        open(filepath, 'w')
        cv2.imwrite(filepath, text_image.get('image'))
        array.append({
            'path': filepath,
            'text': text_image.get('text'),
            'color': text_image.get('color'),
            'bg': text_image.get('bg')
        })

def closest_colour(requested_colour):
    min_colours = {}
    for key, name in webcolors.css3_hex_to_names.items():
        r_c, g_c, b_c = webcolors.hex_to_rgb(key)
        rd = (r_c - requested_colour[0]) ** 2
        gd = (g_c - requested_colour[1]) ** 2
        bd = (b_c - requested_colour[2]) ** 2
        min_colours[(rd + gd + bd)] = name
    return min_colours[min(min_colours.keys())]

def get_colour_name(requested_colour):
    try:
        closest_name = actual_name = webcolors.rgb_to_name(requested_colour)
    except ValueError:
        closest_name = closest_colour(requested_colour)
        actual_name = None
    return actual_name, closest_name

def create_directory_if_not_exists(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

if __name__ == "__main__":
    create_directory_if_not_exists('captcha')
    for i in range(3, 13):
        create_directory_if_not_exists('captcha/{level}'.format(level=i))
        generate_set_images('captcha/{level}'.format(level=i), i, int(sys.argv[1]))
        json.dump(array, codecs.open("path.json", 'w', encoding='utf-8'), separators=(',', ':'), sort_keys=True, indent=4)
        print('Generated set for {level} level.'.format(level=i))
