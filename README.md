# science-practice-generation-svc

Science Practice Part

Service of generation content for integration with azure computer vision

To run project 

```bash

python3 generate.py 100 
# Where 100 is amount of images which should be generated
# The half of them will have white and other part black background colors

```

Then will be created `captcha` folder with content
and `path.json` file with all metadata about images

You can find info about path, value, colours there.